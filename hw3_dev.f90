!M3C 2018 Homework 3
!This module contains four module variables and two subroutines;
!one of these routines must be developed for this assignment.
!Module variables--
! tr_b, tr_e, tr_g: the parameters b, e, and g in the tribe competition model
! numthreads: The number of threads that should be used in parallel regions within simulate2_omp
!
!Module routines---
! simulate2_f90: Simulate tribal competition over m trials. Return: all s matrices at final time
! and fc at nt+1 times averaged across the m trials.
! simulate2_omp: Same input/output functionality as simulate2.f90 but parallelized with OpenMP

module tribes
  use omp_lib
  implicit none
  integer :: numthreads
  real(kind=8) :: tr_b,tr_e,tr_g
contains

!Simulate m trials of Cooperator vs. Mercenary competition using the parameters, tr_b and tr_e.
!Input:
! n: defines n x n grid of villages
! nt: number of time steps
! m: number of trials
!Output:
! s: status matrix at final time step for all m trials
! fc_ave: fraction of cooperators at each time step (including initial condition)
! averaged across the m trials
subroutine simulate2_f90(n,nt,m,s,fc_ave)
  implicit none
  integer :: t1,t2,clock_rate
  integer, intent(in) :: n,nt,m
  integer, intent(out), dimension(n,n,m) :: s
  real(kind=8), intent(out), dimension(nt+1) :: fc_ave
  integer :: i1,j1
  real(kind=8) :: n2inv
  integer, dimension(n,n,m) :: nb,nc
  integer, dimension(n+2,n+2,m) :: s2
  real(kind=8), dimension(n,n,m) :: f,p,a,pden,nbinv
  real(kind=8), dimension(n+2,n+2,m) :: f2,f2s2
  real(kind=8), allocatable, dimension(:,:,:) :: r !random numbers

  call system_clock(t1)
  !---Problem setup----
  !Initialize arrays and problem parameters

  !initial condition
  !initial condition
  s=1
  j1 = (n+1)/2
  s(j1,j1,:) = 0

  n2inv = 1.d0/dble(n*n)
  fc_ave(1) = sum(s)*(n2inv/m)

  s2 = 0
  f2 = 0.d0

  !Calculate number of neighbors for each point
  nb = 8
  nb(1,2:n-1,:) = 5
  nb(n,2:n-1,:) = 5
  nb(2:n-1,1,:) = 5
  nb(2:n-1,n,:) = 5
  nb(1,1,:) = 3
  nb(1,n,:) = 3
  nb(n,1,:) = 3
  nb(n,n,:) = 3

  nbinv = 1.d0/nb
  allocate(r(n,n,m))
  !---finished Problem setup---


  !----Time marching----
  do i1=1,nt

    call random_number(r) !Random numbers used to update s every time step

    !Set up coefficients for fitness calculation in matrix, a
    a = 1
    where(s==0)
      a=tr_b
    end where

    !create s2 by adding boundary of zeros to s
    s2(2:n+1,2:n+1,:) = s

    !Count number of C neighbors for each point
    nc = s2(1:n,1:n,:) + s2(1:n,2:n+1,:) + s2(1:n,3:n+2,:) + &
         s2(2:n+1,1:n,:)                  + s2(2:n+1,3:n+2,:) + &
         s2(3:n+2,1:n,:)   + s2(3:n+2,2:n+1,:)   + s2(3:n+2,3:n+2,:)

    !Calculate fitness matrix, f----
    f = nc*a
    where(s==0)
      f = f + (nb-nc)*tr_e
    end where
    f = f*nbinv
    !-----------

    !Calculate probability matrix, p----
    f2(2:n+1,2:n+1,:) = f
    f2s2 = f2*s2

    !Total fitness of cooperators in community
    p = f2s2(1:n,1:n,:) + f2s2(1:n,2:n+1,:) + f2s2(1:n,3:n+2,:) + &
           f2s2(2:n+1,1:n,:) + f2s2(2:n+1,2:n+1,:)  + f2s2(2:n+1,3:n+2,:) + &
          f2s2(3:n+2,1:n,:)   + f2s2(3:n+2,2:n+1,:)   + f2s2(3:n+2,3:n+2,:)

    !Total fitness of all members of community
    pden = f2(1:n,1:n,:) + f2(1:n,2:n+1,:) + f2(1:n,3:n+2,:) + &
           f2(2:n+1,1:n,:) + f2(2:n+1,2:n+1,:)  + f2(2:n+1,3:n+2,:) + &
          f2(3:n+2,1:n,:)   + f2(3:n+2,2:n+1,:)   + f2(3:n+2,3:n+2,:)


    p = (p/pden)*tr_g + 0.5d0*(1.d0-tr_g) !probability matrix
    !----------

    !Set new affiliations based on probability matrix and random numbers stored in R
    s = 0
    where (R<=p)
        s = 1
    end where

    fc_ave(i1+1) = sum(s)*(n2inv/m)

  end do
  call system_clock(t2,clock_rate)
  print *, dble(t2-t1)/dble(clock_rate)


end subroutine simulate2_f90

!Simulate m trials of Cooperator vs. Mercenary competition using the parameters, tr_b and tr_e.
!Same functionality as simulate2_f90, but parallelized with OpenMP
!Parallel regions should use numthreads threads.
!Input:
! n: defines n x n grid of villages
! nt: number of time steps
! m: number of trials
!Output:
! s: status matrix at final time step for all m trials
! fc_ave: fraction of cooperators at each time step (including initial condition)
! averaged across the m trials
subroutine simulate2_omp(n,nt,m,s,fc_ave)
  implicit none
  integer, intent(in) :: n,nt,m
  integer, intent(out), dimension(n,n,m) :: s
  real(kind=8), intent(out), dimension(nt+1) :: fc_ave
  integer :: t,i1,j1,k1,t1,t2,clock_rate
  real(kind=8) :: n2inv,sum_1,sum_2
  integer, dimension(n,n,m) :: nb,nc
  integer, dimension(n+2,n+2,m) :: s2
  real(kind=8), dimension(n,n,m) :: f,p,a,pden,nbinv
  real(kind=8), dimension(n+2,n+2,m) :: f2,f2s2
  real(kind=8), allocatable, dimension(:,:,:) :: r !random numbers
  !Add further variables as needed

  !$call omp_set_num_threads(numthreads)

  !initial condition and r allocation (does not need to be parallelized)

  call system_clock(t1)
  s=1
  j1 = (n+1)/2
  s(j1,j1,:) = 0

  sum_2=0
  !$OMP parallel do reduction (+:sum_2) collapse(3)
  do k1=1,m
    do j1=1,n
      do i1=1,n
         sum_2=sum_2+s(i1,j1,k1)
       end do
    end do
  end do
  !$OMP end parallel do
  n2inv = 1.d0/dble(n*n)
  fc_ave(1) = sum_2*(n2inv/m)


  !$OMP parallel do collapse(3)
  do k1=1,m
    do j1=1,n+2
      do i1=1,n+2
         s2(i1,j1,k1)=0
         f2(i1,j1,k1)=0
       end do
    end do
  end do
  !$OMP end parallel do


  !Calculate number of neighbors for each point
  !$OMP parallel do collapse(3)
  do k1=1,m
    do j1=2,n-1
      do i1=2,n-1
        nb(i1,j1,k1)=8
        nbinv(i1,j1,k1)=dble(1)/dble(8)
      end do
    end do
  end do
  !$OMP end parallel do

  !$OMP parallel do collapse(2)
  do k1=1,m
    do i1=2,n-1
      nb(i1,1,k1) = 5
      nbinv(i1,1,k1)=dble(1)/dble(5)
      nb(i1,n,k1) = 5
      nbinv(i1,n,k1)=dble(1)/dble(5)
      nb(1,i1,k1) = 5
      nbinv(1,i1,k1)=dble(1)/dble(5)
      nb(n,i1,k1) = 5
      nbinv(n,i1,k1)=dble(1)/dble(5)
    end do
  end do
  !$OMP end parallel do


  !$OMP parallel do
  do k1=1,m
    nb(1,1,k1) = 3
    nbinv(1,1,k1)=dble(1)/dble(3)
    nb(1,n,k1) = 3
    nbinv(1,n,k1)=dble(1)/dble(3)
    nb(n,1,k1) = 3
    nbinv(n,1,k1)=dble(1)/dble(3)
    nb(n,n,k1) = 3
    nbinv(n,n,k1)=dble(1)/dble(3)
  end do
  !$OMP end parallel do




  !---finished Problem setup---
  allocate(r(n,n,m))
  do t=1,nt  !Try parallelization

    call random_number(r) !parallize?
    !Set up coefficients for fitness calculation in matrix, a




    !create s2 by adding boundary of zeros to s
    !$OMP parallel do collapse (3)
    do k1=1,m
      do j1=1,n
        do i1=1,n
          if (s(i1,j1,k1)==0) then
            a(i1,j1,k1)=tr_b
          else
            a(i1,j1,k1)=1
          end if
          s2(i1+1,j1+1,k1) = s(i1,j1,k1)
        end do
      end do
    end do
    !$OMP end parallel do

    !Count number of C neighbors for each point
    !try using reduction


    !Calculate fitness matrix, f----

    !-----------

    !Calculate probability matrix, p----
    !try using reduction
    !$OMP parallel do collapse(3)
    do k1=1,m
      do j1=1,n
        do i1=1,n
          nc(i1,j1,k1) = s2(i1,j1,k1) + s2(i1,j1+1,k1) + s2(i1,j1+2,k1) + &
               s2(i1+1,j1,k1)                  + s2(i1+1,j1+2,k1) + &
               s2(i1+2,j1,k1)   + s2(i1+2,j1+1,k1)   + s2(i1+2,j1+2,k1)

          f(i1,j1,k1)=nbinv(i1,j1,k1)*(nc(i1,j1,k1)*a(i1,j1,k1)-(nb(i1,j1,k1)-nc(i1,j1,k1))*(tr_e)*(s(i1,j1,k1)-1))
          f2(i1+1,j1+1,k1)=f(i1,j1,k1)
          f2s2(i1+1,j1+1,k1)=f2(i1+1,j1+1,k1)*s2(i1+1,j1+1,k1)
        end do
      end do
    end do
    !$OMP end parallel do


    !Total fitness of cooperators in community
    sum_1=0
    !$OMP parallel do reduction (+:sum_1) collapse(3)
    do k1=1,m
      do j1=1,n
        do i1=1,n

          p(i1,j1,k1) = f2s2(i1,j1,k1) + f2s2(i1,j1+1,k1) + f2s2(i1,j1+2,k1) + &
               f2s2(i1+1,j1,k1)+f2s2(i1+1,j1+1,k1) + f2s2(i1+1,j1+2,k1) + &
               f2s2(i1+2,j1,k1)   + f2s2(i1+2,j1+1,k1)   + f2s2(i1+2,j1+2,k1)

          pden(i1,j1,k1) = f2(i1,j1,k1) + f2(i1,j1+1,k1) + f2(i1,j1+2,k1) + &
               f2(i1+1,j1,k1)+f2(i1+1,j1+1,k1) + f2(i1+1,j1+2,k1) + &
              f2(i1+2,j1,k1)   + f2(i1+2,j1+1,k1)   + f2(i1+2,j1+2,k1)

          p(i1,j1,k1) = (p(i1,j1,k1)/pden(i1,j1,k1))*tr_g + 0.5d0*(1.d0-tr_g)

          if (r(i1,j1,k1)<=p(i1,j1,k1)) then
            s(i1,j1,k1)=1
          else
            s(i1,j1,k1)=0
          end if
          sum_1=sum_1+s(i1,j1,k1)

        end do
      end do
    end do
    !$OMP end parallel do


    !Total fitness of all members of community
!probability matrix
    !----------





    fc_ave(t+1) = sum_1*(n2inv/m)







  end do
  call system_clock(t2,clock_rate)
  print *, dble(t2-t1)/dble(clock_rate)

  !------------------

  !Add code here


  deallocate(r)
end subroutine simulate2_omp


end module tribes
