"""M3C 2018 Homework 3
Contains five functions:
    plot_S: plots S matrix -- use if you like
    simulate2: Simulate tribal competition over m trials. Return: all s matrices at final time
        and fc at nt+1 times averaged across the m trials.
    performance: To be completed -- analyze and assess performance of python, fortran, and fortran+openmp simulation codes
    analyze: To be completed -- analyze influence of model parameter, g
    visualize: To be completed -- generate animation illustrating "interesting" tribal dynamics
"""
import numpy as np
import matplotlib.pyplot as plt
import time
from m1 import tribes as tr #assumes that hw3_dev.f90 has been compiled with: f2py --f90flags='-fopenmp' -c hw3_dev.f90 -m m1 -lgomp
#May also use scipy and time modules as needed


def plot_S(S):
    """Simple function to create plot from input S matrix
    """
    ind_s0 = np.where(S==0) #C locations
    ind_s1 = np.where(S==1) #M locations
    plt.plot(ind_s0[1],ind_s0[0],'rs')
    plt.plot(ind_s1[1],ind_s1[0],'bs')
    plt.show()
    return None
#------------------


def simulate2(N,Nt,b,e,g,m):
    """Simulate m trials of C vs. M competition on N x N grid over
    Nt generations. b, e, and g are model parameters
    to be used in fitness calculations.
    Output: S: Status of each gridpoint at end of simulation, 0=M, 1=C
            fc_ave: fraction of villages which are C at all Nt+1 times
                    averaged over the m trials
    """
    #Set initial condition
    S  = np.ones((N,N,m),dtype=int) #Status of each gridpoint: 0=M, 1=C
    j = int((N-1)/2)
    S[j,j,:] = 0
    N2inv = 1./(N*N)

    fc_ave = np.zeros(Nt+1) #Fraction of points which are C
    fc_ave[0] = S.sum()

    #Initialize matrices
    NB = np.zeros((N,N,m),dtype=int) #Number of neighbors for each point
    NC = np.zeros((N,N,m),dtype=int) #Number of neighbors who are Cs
    S2 = np.zeros((N+2,N+2,m),dtype=int) #S + border of zeros
    F = np.zeros((N,N,m)) #Fitness matrix
    F2 = np.zeros((N+2,N+2,m)) #Fitness matrix + border of zeros
    A = np.ones((N,N,m)) #Fitness parameters, each of N^2 elements is 1 or b
    P = np.zeros((N,N,m)) #Probability matrix
    Pden = np.zeros((N,N,m))
    #---------------------

    #Calculate number of neighbors for each point
    NB[:,:,:] = 8
    NB[0,1:-1,:],NB[-1,1:-1,:],NB[1:-1,0,:],NB[1:-1,-1,:] = 5,5,5,5
    NB[0,0,:],NB[-1,-1,:],NB[0,-1,:],NB[-1,0,:] = 3,3,3,3
    NBinv = 1.0/NB
    #-------------

    #----Time marching-----
    for t in range(Nt):
        R = np.random.rand(N,N,m) #Random numbers used to update S every time step

        #Set up coefficients for fitness calculation
        A = np.ones((N,N,m))
        ind0 = np.where(S==0)
        A[ind0] = b

        #Add boundary of zeros to S
        S2[1:-1,1:-1,:] = S

        #Count number of C neighbors for each point
        NC = S2[:-2,:-2,:]+S2[:-2,1:-1,:]+S2[:-2,2:,:]+S2[1:-1,:-2,:] + S2[1:-1,2:,:] + S2[2:,:-2,:] + S2[2:,1:-1,:] + S2[2:,2:,:]

        #Calculate fitness matrix, F----
        F = NC*A
        F[ind0] = F[ind0] + (NB[ind0]-NC[ind0])*e
        F = F*NBinv
        #-----------

        #Calculate probability matrix, P-----
        F2[1:-1,1:-1,:]=F
        F2S2 = F2*S2
        #Total fitness of cooperators in community
        P = F2S2[:-2,:-2,:]+F2S2[:-2,1:-1,:]+F2S2[:-2,2:,:]+F2S2[1:-1,:-2,:] + F2S2[1:-1,1:-1,:] + F2S2[1:-1,2:,:] + F2S2[2:,:-2,:] + F2S2[2:,1:-1,:] + F2S2[2:,2:,:]

        #Total fitness of all members of community
        Pden = F2[:-2,:-2,:]+F2[:-2,1:-1,:]+F2[:-2,2:,:]+F2[1:-1,:-2,:] + F2[1:-1,1:-1,:] + F2[1:-1,2:,:] + F2[2:,:-2,:] + F2[2:,1:-1,:] + F2[2:,2:,:]

        P = (P/Pden)*g + 0.5*(1.0-g) #probability matrix
        #---------

        #Set new affiliations based on probability matrix and random numbers stored in R
        S[:,:,:] = 0
        S[R<=P] = 1

        fc_ave[t+1] = S.sum()
        #----Finish time marching-----

    fc_ave = fc_ave*N2inv/m

    return S,fc_ave
#------------------


def performance(input=(None),display=False):
    """The performances of openMP and Fortran are much better than Python alone
    from the three graphs provided. The increasing rate of time of Python with each variable
    is also greater than OpenMP and Fortran. Espeically when it comes to the
    matrix size, the run-time of python code seems increasing more quickly. The run time
    of fortran and openMP codes are much closer. When the variable sizes are small, we
    can see than the the run time of Fortran is even greater than the run time of 22threads
    OMP. This is because the run time is even smaller than processing time of openMP (fork and merge)
    when variables like Nt,N and m are small. However, when variable sizes become larger and larger,
    the differences between the bottom two lines also widens, but the difference is not as
    large as with python's run time.

    Generally, 2-thread is around 30% percent quicker than 1-thread Fortran version since
    there are certain areas that cannot be parallelized and some overhead time in processing
    fork and merges.

    THe graphs show that the run time of each method seems more sensitive to the Matrix
    size and they fluctuate a lot when number of trials increase. It is not surprising
    because every time we double the matrix size, the actual amount of data will increase by
    4 times.

    """
    tr.numthreads=2
    tr.tr_e=0.01
    tr.tr_b=1.1
    tr.tr_g=0.95
    for i in range(3):
        omp_time_1=[]
        fortran_time=[]
        py_time=[]
        figure=plt.figure()
        if i==0:
            nt0=1000
            m0=50
            n=np.linspace(21,50,30)
            for j in range(len(n)):
                start_time=time.time()
                tr.simulate2_f90(n[j],nt0,m0)
                elapsed_time_fortran=time.time()-start_time
                fortran_time.append(elapsed_time_fortran)
                start_time=time.time()
                tr.simulate2_omp(n[j],nt0,m0)
                elapsed_time_omp=time.time()-start_time
                omp_time_1.append(elapsed_time_omp)
                start_time=time.time()
                simulate2(int(n[j]),nt0,1.1,0.01,0.95,m0)
                elapsed_time_py=time.time()-start_time
                py_time.append(elapsed_time_py)
            plt.plot(n,fortran_time,label="Py+Fortran")
            plt.plot(n,omp_time_1,label="Fortran-2threads")
            plt.plot(n,py_time,label="Py")
            plt.legend()
            plt.xlabel("Matrix Size")
            plt.ylabel("Time/s")
            plt.title("m=50,Nt=1000, Time vs Matrix Size")
        if i==1:
            n0=21
            m0=50
            nt=np.linspace(50,1050,21)
            for j in range(len(nt)):
                start_time=time.time()
                tr.simulate2_f90(n0,nt[j],m0)
                elapsed_time_fortran=time.time()-start_time
                fortran_time.append(elapsed_time_fortran)
                start_time=time.time()
                tr.simulate2_omp(n0,nt[j],m0)
                elapsed_time_omp=time.time()-start_time
                omp_time_1.append(elapsed_time_omp)
                start_time=time.time()
                simulate2(n0,int(nt[j]),1.1,0.01,0.95,m0)
                elapsed_time_py=time.time()-start_time
                py_time.append(elapsed_time_py)
            plt.plot(nt,fortran_time,label="Py+Fortran")
            plt.plot(nt,omp_time_1,label="Fortran-2threads")
            plt.plot(nt,py_time,label="Py")
            plt.legend()
            plt.xlabel("Number of Generations")
            plt.ylabel("Time/s")
            plt.title("N=21,m=50, Time vs Number of Generations")


        if i==2:
            n0=21
            m=np.linspace(11,50,40)
            nt0=1000
            for j in range(len(m)):
                start_time=time.time()
                tr.simulate2_f90(n0,nt0,int(m[j]))
                elapsed_time_fortran=time.time()-start_time
                fortran_time.append(elapsed_time_fortran)
                start_time=time.time()
                tr.simulate2_omp(n0,nt0,int(m[j]))
                elapsed_time_omp=time.time()-start_time
                omp_time_1.append(elapsed_time_omp)
                start_time=time.time()
                simulate2(n0,nt0,1.1,0.01,0.95,int(m[j]))
                elapsed_time_py=time.time()-start_time
                py_time.append(elapsed_time_py)
            plt.plot(m,fortran_time,label="Py+Fortran")
            plt.plot(m,omp_time_1,label="Fortran-2threads")
            plt.plot(m,py_time,label="Py")
            plt.legend()
            plt.title("N=21,Nt=1000, Time vs Number of Trails")
            plt.xlabel("Number of Trials")
            plt.ylabel("Time/s")

    plt.show()



    return

def analyze(input=(None),display=False):
    """The three graphs shown different g values under gamma=0.8,0.9 and 1.0
    Generally, in the area of 0.8 to 1.0, given a fixed gamma, the final percentrage of
    collabrator increases as b decreases, with a maximum of around 40% for b up to 1.5
    In addition, the percentage of collabrators drop quickly to stablized level after 50
    generations

    HOWEVER, when gamma is close to 1, the pattern breaks. Although the pattern metioned
    above still preserves, different values of b may cross with each other after 100 generations
    When gamma is closer to 1, the probability of becoming C is identical to the Probability
    in the first homework1, therefore, it is not surprising that they should give the similiar results
    for different values of b.
    """
    tr.numthreads=2
    tr.tr_e=0.01
    nt=500
    n=21
    m=100
    g=np.array([0.8,0.9,1.0])
    b=np.linspace(1,1.5,11)
    nt_array=np.linspace(0,nt+1,nt+1)

    for i in range(len(g)):
        tr.tr_g=g[i]
        figure=plt.figure()
        for j in range(len(b)):
            tr.tr_b=b[j]
            s,fc_array=tr.simulate2_f90(n,nt,m)
            plt.plot(nt_array,fc_array,label='b = %s' % b[j])
            plt.legend(prop={"size":6})
        plt.title("Percentage of Collaborators for Gamma = %s" %g[i])
        plt.xlabel("Number of Generations")
        plt.ylabel("Percentage of Collaborators")
        plt.tight_layout()
    plt.show()

    return



def visualize():
    """Generate an animation illustrating the evolution of
        villages during C vs M competition
    """
    tr.tr_b=1.2
    tr.tr_e=0.01
    tr.tr_g=0.8
    S,S_array=tr.simulate2_f90_visualize(21,1000,50)
    fig,ax= plt.subplots()
    line= plot_S(S_array[:,:,0])
    ax.set_xlim(1,21)
    ax.set_ylim(1,21)

    def update(i):
        print (i)

        line=plot_S(S_array[:,:,i+1])

        return line

    ani = animation.FuncAnimation(fig, update, frames=30,interval=100,repeat=False)
    plt.show()
    ani.save("animation.mp4",writer="ffmpeg")


    return ani

if __name__ == '__main__':
    #Modify the code here so that it calls performance analyze and
    # generates the figures that you are submitting with your code


    input_a = None
    output_a = performance(input_a)
